import React ,{Component} from 'react';
import {View,Text,Button,TextInput,StyleSheet,ImageBackground,Dimensions,KeyboardAvoidingView,
Keyboard,ActivityIndicator,
  TouchableWithoutFeedback} from 'react-native';

import DefaultInput from "../../component/UI/DefaultInput/DefaultInput"
import HeadingText from "../../component//UI/HeadingText/HeadingText";
import MainText from "../../component//UI/MainText/MainText";
import backgoundimage from "../../assets/mountain.jpg"
import ButtonWithBackground from "../../component/UI/ButtonWithBackground/ButtonWithBackground";
import validate from '../../utility/validation';
import {connect} from 'react-redux'
import {tryAuth} from '../../store/actions/index'
class AuthScreen extends Component{
    state ={
    viewMode:Dimensions.get("window").height>500?"portrait":"landscape",
    authMode:"Login",
    constrols:{
        email:{
            value:"",
            valid:false,
            validationRule:{
                isEmail:true
            },
            touched:false

        },
        password:
        {
            value:"",
            valid:false,
            validationRule:{
            minLength:6
        },
        touched:false
        },
        confirmPassword:{
            
            value:"",
            valid:false,
            validationRule:{
            equalTo:'password'
        },
        touched:false
    }
    }

    }
    updateInputState = (key,value) =>{
       
        let connectedValue={};
        if(this.state.constrols[key].validationRule.equalTo){
            const equalControl=this.state.constrols[key].validationRule.equalTo;                    
            const equalValue =this.state.constrols[equalControl].value;
            console.log(equalValue);
            connectedValue={
                ...connectedValue,
                equalTo:equalValue               
            }
        }
        if(key==='password'){
            
            //console.log(equalValue);
            connectedValue={
                ...connectedValue,
                equalTo:value               
            }

        }
    this.setState(prevState =>{
        return{ 
            constrols:{
                
                ...prevState.constrols,
                confirmPassword:{
                    ...prevState.constrols.confirmPassword,
                    valid: key==='password'? validate(prevState.constrols.confirmPassword.value,
                        prevState.constrols.confirmPassword.validationRule,
                        connectedValue): 
                    prevState.constrols.confirmPassword.valid
                },
                [key]:{
                    ...prevState.constrols[key],
                    value:value, 
                    valid :validate(value,prevState.constrols[key].validationRule,connectedValue) ,
                    touched:true              
                }
               
               
                
            }
        } 
    })
    }
constructor (props){
    super(props);
    Dimensions.addEventListener("change",this.updateStyle)

}
componentWillUnmount(){
    Dimensions.removeEventListener("change",this.updateStyle);
}
swithAuthModeHandler = () =>{
    this.setState(prevState=>{
        return{
            authMode:prevState.authMode ==="Login"?"signup":"Login"
        }
    });
}

updateStyle =(dims) =>{
    this.setState({
        viewMode:dims.window.height>500?"portrait":"landscape"
       })
}
    authHandler=() =>{
        const authData ={
            email:this.state.constrols.email.value,
            password:this.state.constrols.password.value
        }
        this.props.onTryAuth(authData,this.state.authMode)
    
    }
    render(){
        let confirmPasswordControl = null;
        let headingText =null;
        let submitButton =(
            <ButtonWithBackground color="#29aaf4" onPress={this.authHandler}
            disable ={!this.state.constrols.email.valid 
                ||!this.state.constrols.password.valid
                 ||!this.state.constrols.confirmPassword.valid && this.state.authMode === "signup"} >
               Submit
            </ButtonWithBackground>
        )
        if(this.state.viewMode==="portrait"){
            headingText=
        (<MainText>
        <HeadingText>Please Log In</HeadingText>
        </MainText>)
        }
        if (this.state.authMode==='signup'){
            confirmPasswordControl =(<View style={
                this.state.viewMode==="portrait"
                ?style.portraitPasswordWrapper
                :style.landscapPasswordWrapper
                }>
            <DefaultInput placeholder="Confirm Password" 
            style={style.input}
            value={this.state.constrols.confirmPassword.value}
            onChangeText={(val)=>this.updateInputState('confirmPassword',val)}
            valid = {this.state.constrols.confirmPassword.valid}
            touched ={this.state.constrols.confirmPassword.touched}
             secureTextEntry/>
            
         
           
            </View>)
        }
        if(this.state.isLoading){
            submitButton =<ActivityIndicator />
        }
        return(
    <ImageBackground source ={backgoundimage} style={style.backgoundImage}>
<KeyboardAvoidingView style={style.container} behavior="padding">
{headingText}

    <ButtonWithBackground 
    onPress ={this.swithAuthModeHandler}
    color="#29aaf4"> 
       switch to {this.state.authMode==='Login'?'Sign up':"Login"}
    </ButtonWithBackground>
  <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
    <View style={style.inputContainer}> 
    <DefaultInput placeholder="Your Email Address" 
    style={style.input}
    value={this.state.constrols.email.value}
    onChangeText={(val)=>this.updateInputState('email',val)}
    valid = {this.state.constrols.email.valid}
    touched ={this.state.constrols.email.touched}
    autoCapitalize="none"
    autoCorrect={false}
    keyboardType="email-address"/>
    <View style={
        this.state.viewMode==="portrait"
        ||this.state.authMode==="Login"
        ?style.portratePasswordContaner
        :style.landScapPasswordContaner}>
    <View style={
        this.state.viewMode==="portrait"
        ||this.state.authMode==="Login"
        ?style.portraitPasswordWrapper
        :style.landscapPasswordWrapper}>
    <DefaultInput placeholder="Password" 
    style={style.input}
    value={this.state.constrols.password.value}
    onChangeText={(val)=>this.updateInputState('password',val)}
    valid = {this.state.constrols.password.valid}
    touched ={this.state.constrols.password.touched}
    secureTextEntry/>
   
    
    
    </View>
    {confirmPasswordControl}
    </View>
    </View>
    </TouchableWithoutFeedback>
   
    {submitButton}
    </KeyboardAvoidingView>
    </ImageBackground>
        ) 
    }
}
const style = StyleSheet.create({
    container :{
        flex:1,
        justifyContent:"center",
        alignItems:"center"
    },
    backgoundImage:{
        width:"100%",
        flex:1
    },
    inputContainer:{
        width:"80%"
    },
    input:{
      backgroundColor:"#eee",
      borderColor:"#bbb"
    },
    landScapPasswordContaner:{
        flexDirection:"row",
        justifyContent:"space-between"
    },
    portratePasswordContaner:{
        flexDirection:"column",
        justifyContent:"flex-start"
    },
    landscapPasswordWrapper:{
        width:"45%"
    },
    portraitPasswordWrapper:{
        width:"100%"
    }
})
const stateToProps=state =>{
    return{
        isLoading:state.ui.isLoading
    }
   
} 
const mapDispatchToProps = dispatch=>{
    return {
        onTryAuth:(authData,authMode)=> dispatch(tryAuth(authData,authMode))
    }
}

export default connect(stateToProps,mapDispatchToProps)(AuthScreen);