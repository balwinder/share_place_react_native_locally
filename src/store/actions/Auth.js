import {TRY_AUTH,AUTH_SET_TOKEN} from './actionTypes';
import {  uiStartLoading,uiStopLoading} from "./index";
import startMainTab from "../../screens/MainTabs/startMainTabs"


export const tryAuth =(authData,authMode) =>{
    return dispatch =>{
        dispatch(uiStartLoading());
       let url ="http://localhost:4000/user/login"
        if(authMode==='signup'){
            url ="http://localhost:4000/user/signup"
        }
        fetch(url,{
             method:"POST",
             headers: {
                'Content-Type': 'application/json'
              },
             body:JSON.stringify( {
                email:authData.email,
                password:authData.password 
             }
                 )
             
        }).then(res => res.json())
        .then(parsedRes=>{
            dispatch(uiStopLoading());
            if(!parsedRes.token){
                alert("Authentication failed, Please try again")
            }else{
               dispatch(authSetToken(parsedRes.token))
                startMainTab()

                console.log(parsedRes)
            }
                
            
           
        }).catch(err=>{
            dispatch(uiStopLoading());
            console.log(err);
            alert("Authentication failed, Please try again end")
        })
       
    }
}
export const authSetToken = token =>{
   return {
       type:AUTH_SET_TOKEN,
       token:token
   }
}
export const authGetToken=()=>{
    return (dispatch,getstate)=>{
        const promise = new Promise((resolve,reject)=>{
            const token = getstate().auth.token;
            if(!token){
                reject();

            }else{
                resolve(token)
            }
        })
      return promise

    }
}