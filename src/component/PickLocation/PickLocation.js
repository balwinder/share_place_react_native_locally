import React,{Component} from 'React'
import {View,Image,Button,StyleSheet,Text,Dimensions} from 'react-native'
import ImagePlace from '../../assets/mountain.jpg'
import MapView from 'react-native-maps'
 class PickImage extends Component{
     state = {
         focusedLocation:{
            
            longitude:-122.4013726,
             latitude:37.7900352,
             latitudeDelta:0.0122,
             longitudeDelta:Dimensions.get("window").width
              / Dimensions.get("window").height
              *0.0122
         },
         locationChosen:false
     }
     pickLocationHandler = event =>{
         const coords = event.nativeEvent.coordinate;
       
         this.map.animateToRegion({
             ...this.state.focusedLocation,
             longitude:coords.longitude,
             latitude:coords.latitude
         })
         this.setState(prevState =>{
             return{
                 focusedLocation:{
                     ...prevState.focusedLocation,
                     longitude:coords.longitude,
                     latitude:coords.latitude
                 },
                 locationChosen:true
             }
         })
         this.props.onPickLocation({
            longitude:coords.longitude,
             latitude:coords.latitude
         })
     }
     getLocationHandler =() =>{
         navigator.geolocation.getCurrentPosition(pos =>{
           const coordsEvents = {
               nativeEvent:{
                   coordinate:{
                       longitude:pos.coords.longitude,
                       latitude:pos.coords.latitude
                   }
               }
           }
          
           this.pickLocationHandler(coordsEvents)
         },
        err=>{
            console.log(err);
            alert("Fething fail pPlkease pick manually")
        }
    )

     }
  render(){
      let marker =null;
      if(this.state.locationChosen){
          marker=<MapView.Marker  coordinate={this.state.focusedLocation}/>
      }
      return(
        <View style={styles.container}>
        <MapView 
        initialRegion={this.state.focusedLocation}
     
        style={styles.map}
        onPress={this.pickLocationHandler}
        ref={ref =>this.map=ref}>
        {marker}
        </MapView>
        <View style={styles.button}>
        <Button title="Place Name" onPress={this.getLocationHandler}/>
        </View> 
        </View>
      );
  }
}
const styles = StyleSheet.create({
    map:{
       width:"100%",
       height:250
    
    },
    button:{
      margin:8
    },
    PreviewImage:{
        width:"100%",
        height:"100%"
    },
    container:{
        width:"100%",
        alignItems:"center"
    }
})
export default PickImage;